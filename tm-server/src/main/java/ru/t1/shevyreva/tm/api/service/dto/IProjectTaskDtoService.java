package ru.t1.shevyreva.tm.api.service.dto;

import org.jetbrains.annotations.Nullable;

public interface IProjectTaskDtoService {

    void bindTaskToProject(@Nullable String userId, @Nullable String projectId, @Nullable String taskId);

    void unbindTaskToProject(@Nullable String userId, @Nullable String projectId, @Nullable String taskId);

    void removeByProjectId(@Nullable String userId, @Nullable String projectId);

}
