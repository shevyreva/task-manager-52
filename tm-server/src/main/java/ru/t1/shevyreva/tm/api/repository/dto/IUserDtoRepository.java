package ru.t1.shevyreva.tm.api.repository.dto;

import org.jetbrains.annotations.NotNull;
import ru.t1.shevyreva.tm.dto.model.UserDTO;

public interface IUserDtoRepository extends IAbstractModelDtoRepository<UserDTO> {

    UserDTO findByLogin(@NotNull final String login);

    UserDTO findByEmail(@NotNull final String email);

    void removeOne(@NotNull final UserDTO user);

}
