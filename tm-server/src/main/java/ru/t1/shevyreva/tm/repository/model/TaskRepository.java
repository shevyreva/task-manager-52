package ru.t1.shevyreva.tm.repository.model;

import org.hibernate.jpa.QueryHints;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.shevyreva.tm.api.repository.model.ITaskRepository;
import ru.t1.shevyreva.tm.model.Task;

import javax.persistence.EntityManager;
import java.util.Comparator;
import java.util.List;

public class TaskRepository extends AbstractUserOwnedModelRepository<Task> implements ITaskRepository {

    public TaskRepository(@NotNull EntityManager entityManager) {
        super(entityManager);
    }

    @Override
    public void clearForUser(@NotNull final String userId) {
        entityManager.remove(findAllForUser(userId));
    }

    @Override
    public void clear() {
        entityManager.remove(findAll());
    }

    @Override
    public List<Task> findAllForUser(@NotNull final String userId) {
        return entityManager.createQuery("SELECT e FROM Task e WHERE e.userId = :userId", Task.class)
                .setParameter("userId", userId)
                .getResultList();
    }

    @Override
    public List<Task> findAll() {
        return entityManager.createQuery("SELECT e FROM Task e", Task.class).getResultList();
    }

    @Override
    public List<Task> findAll(@Nullable final Comparator<Task> comparator) {
        @NotNull final String sortType = getSortType(comparator);
        return entityManager.createQuery("SELECT e FROM Task e ORDER BY e." + sortType, Task.class).getResultList();
    }

    @Override
    public List<Task> findAllForUser(@NotNull final String userId, @Nullable final Comparator<Task> comparator) {
        @NotNull final String sortType = getSortType(comparator);
        return entityManager.createQuery("SELECT e FROM Task e WHERE e.userId = :userId ORDER BY e." + sortType, Task.class)
                .setHint(QueryHints.HINT_CACHEABLE, true)
                .setParameter("userId", userId)
                .getResultList();
    }

    @Override
    public Task findOneByIdForUser(@NotNull final String userId, @NotNull final String id) {
        return entityManager.createQuery("SELECT e FROM Task e WHERE e.id = :id AND e.userId = :userId", Task.class)
                .setHint(QueryHints.HINT_CACHEABLE, true)
                .setParameter("userId", userId)
                .setParameter("id", id)
                .getSingleResult();
    }

    @Nullable
    @Override
    public Task findOneById(@NotNull final String id) {
        return entityManager.find(Task.class, id);
    }

    @Override
    public Task findOneByIndexForUser(@NotNull final String userId, @NotNull final Integer index) {
        return entityManager.createQuery("SELECT e FROM Task e WHERE e.userId = :userId", Task.class)
                .setParameter("userId", userId)
                .setFirstResult(index)
                .setMaxResults(1)
                .getResultList().stream().findFirst().orElse(null);
    }

    @Override
    public Task findOneByIndex(@NotNull final Integer index) {
        return entityManager.createQuery("SELECT e FROM Task e", Task.class)
                .setFirstResult(index)
                .setMaxResults(1)
                .getResultList().stream().findFirst().orElse(null);
    }


    @Override
    public int getSize() {
        return entityManager.createQuery("SELECT COUNT(e) FROM Task e", Integer.class)
                .setMaxResults(1)
                .getSingleResult();
    }

    @Override
    public int getSizeForUser(@NotNull final String userId) {
        return entityManager.createQuery("SELECT COUNT(e) FROM Task e WHERE e.userId = :userId", Integer.class)
                .setMaxResults(1)
                .getSingleResult();
    }

    @Override
    public void removeOneByIdForUser(@NotNull final String userId, @NotNull final String id) {
        entityManager.remove(findOneByIdForUser(userId, id));
    }

    @Override
    public void removeOneById(@NotNull String id) {
        entityManager.remove(findOneById(id));
    }

    @Override
    public boolean existsByIdForUser(@NotNull final String userId, @NotNull final String id) {
        return findOneByIdForUser(userId, id) != null;
    }

    @Override
    public boolean existById(@NotNull final String id) {
        return findOneById(id) != null;
    }

    public List<Task> findAllByProjectId(@NotNull final String userId, @NotNull final String projectId) {
        return entityManager.createQuery("SELECT e FROM Task e WHERE e.projectId = :projectId AND e.userId = :userId", Task.class)
                .setParameter("userId", userId)
                .setParameter("projectId", projectId)
                .getResultList();
    }
}
