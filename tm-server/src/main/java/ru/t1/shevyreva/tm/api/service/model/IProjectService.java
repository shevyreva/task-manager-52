package ru.t1.shevyreva.tm.api.service.model;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.shevyreva.tm.enumerated.ProjectSort;
import ru.t1.shevyreva.tm.enumerated.Status;
import ru.t1.shevyreva.tm.model.Project;

import java.util.Collection;
import java.util.List;

public interface IProjectService {


    @NotNull
    @SneakyThrows
    Project create(@Nullable String userId, @Nullable String name, @Nullable String description);

    @NotNull
    @SneakyThrows
    Project create(@Nullable String userId, @Nullable String name);

    @NotNull
    @SneakyThrows
    Project updateByIndex(@Nullable String userId, @Nullable Integer index, @Nullable String name, @Nullable String description);

    @NotNull
    @SneakyThrows
    Project updateById(@Nullable String userId, @Nullable String id, @Nullable String name, @Nullable String description);

    @NotNull
    @SneakyThrows
    Project changeProjectStatusByIndex(@Nullable String userId, @Nullable Integer Index, @Nullable Status status);

    @NotNull
    @SneakyThrows
    Project changeProjectStatusById(@Nullable String userId, @Nullable String id, @Nullable Status status);

    @NotNull
    @SneakyThrows
    List<Project> findAll(@Nullable String userId, @Nullable ProjectSort sort);

    @NotNull
    @SneakyThrows
    Project findOneById(@Nullable final String userId, @Nullable final String id);

    @NotNull
    @SneakyThrows
    Project findOneByIndex(@Nullable final String userId, @Nullable final Integer index);

    int getSize(@Nullable final String userId);

    @NotNull
    @SneakyThrows
    Project removeOne(@Nullable final String userId, @Nullable final Project project);

    @NotNull
    @SneakyThrows
    Project removeOneById(@Nullable final String userId, @Nullable final String id);

    void removeAll(@Nullable final String userId);

    @NotNull
    @SneakyThrows
    Collection<Project> set(@NotNull final Collection<Project> models);

    @SneakyThrows
    void removeAll();

    @NotNull
    @SneakyThrows
    Collection<Project> add(@NotNull final Collection<Project> models);

    @NotNull
    @SneakyThrows
    Project add(@NotNull final Project model);

    @SneakyThrows
    List<Project> findAll();

    boolean existsById(@NotNull final String user_id, @NotNull final String id);

}
