package ru.t1.shevyreva.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.t1.shevyreva.tm.dto.request.task.TaskUnbindToProjectRequest;
import ru.t1.shevyreva.tm.util.TerminalUtil;

public class TaskUnbindFromProjectCommand extends AbstractTaskCommand {

    @NotNull
    private final String DESCRIPTION = "Unbind task from a project.";

    @NotNull
    private final String NAME = "task-unbind-from-project";

    @NotNull
    @Override
    public String getName() {
        return this.NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return this.DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[UNBIND TASK FROM PROJECT]");
        System.out.println("Enter project id:");
        @NotNull final String projectId = TerminalUtil.nextLine();
        System.out.println("Enter task id:");
        @NotNull final String taskId = TerminalUtil.nextLine();
        @NotNull final TaskUnbindToProjectRequest request = new TaskUnbindToProjectRequest(getToken(), taskId, projectId);
        getTaskEndpoint().unbindTaskFromProject(request);
    }

}
