package ru.t1.shevyreva.tm.command.user;

import org.jetbrains.annotations.Nullable;
import ru.t1.shevyreva.tm.api.endpoint.IAuthEndpoint;
import ru.t1.shevyreva.tm.api.endpoint.IUserEndpoint;
import ru.t1.shevyreva.tm.command.AbstractCommand;
import ru.t1.shevyreva.tm.dto.model.UserDTO;
import ru.t1.shevyreva.tm.exception.entity.UserNotFoundException;

public abstract class AbstractUserCommand extends AbstractCommand {

    @Nullable
    protected IAuthEndpoint getAuthEndpoint() {
        if (getServiceLocator() == null) return null;
        return getServiceLocator().getAuthEndpoint();
    }

    @Nullable
    protected IUserEndpoint getUserEndpoint() {
        return getServiceLocator().getUserEndpoint();
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    protected void showUser(@Nullable final UserDTO user) {
        if (user == null) throw new UserNotFoundException();
        System.out.println("Login: " + user.getLogin());
        System.out.println("Id: " + user.getId());
    }

}
